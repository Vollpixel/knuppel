;;;; Knuppel.asd

(asdf:defsystem #:knuppel
  :description "Knuppel - An implementation of Kniffel"
  :author "Peter Stoldt <peter-stoldt@web.de>"
  :license  "GNU General Public License version 3"
  :version "2.0"
  :serial t
  :components ((:file "package")
               (:file "knuppel")))
