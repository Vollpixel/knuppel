;; (setf sb-impl::*default-external-format* :utf-8)

;; (ql:quickload :knuppel)
(in-package :knuppel)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                 Knuppel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Description:  Knuppel is an open source implementation of the
;;               Yahzee / Kniffel dice game - implemented in Common Lisp.
;;               The current version uses a vt100 terminal for the user
;;               interface. A keyboard is needed to enter player names and
;;               to control the game.
;;
;; Copyright (C) 2020 - 2023, Peter Stoldt
;;
;; License:      GPL3
;;
;;       1.0     1st playable version
;;       2.0     Use vt100 control sequences for better presentation
;;       2.2     Add save / load game
;;       2.2.1   Fixed problems
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defparameter *tool-name* "knuppel")
(defparameter *tool-author* "Peter Stoldt")
(defparameter *tool-version* "2.2.1")
(defparameter *tool-last-modified* "13-Aug-2023")
(defparameter *version* (format nil "~a ~a [~a] -- (c) 2020-2023 by ~a"
                                *tool-name* *tool-version*
                                *tool-last-modified* *tool-author*))

;; System
;;;;;;;;;;;

(defparameter *files-dir* "~/.knuppel/")

;; Dice
;;;;;;;;;

(defparameter *dice-one* '("╭───────╮"
                           "│       │"
                           "│   ●   │"
                           "│       │"
                           "╰───────╯"))

(defparameter *dice-two* '("╭───────╮"
                           "│     ● │"
                           "│       │"
                           "│ ●     │"
                           "╰───────╯"))

(defparameter *dice-three* '("╭───────╮"
                             "│     ● │"
                             "│   ●   │"
                             "│ ●     │"
                             "╰───────╯"))

(defparameter *dice-four* '("╭───────╮"
                            "│ ●   ● │"
                            "│       │"
                            "│ ●   ● │"
                            "╰───────╯"))

(defparameter *dice-five* '("╭───────╮"
                            "│ ●   ● │"
                            "│   ●   │"
                            "│ ●   ● │"
                            "╰───────╯"))

(defparameter *dice-six* '("╭───────╮"
                           "│ ●   ● │"
                           "│ ●   ● │"
                           "│ ●   ● │"
                           "╰───────╯"))

(defparameter *dice* (list *dice-one* *dice-two* *dice-three*
                           *dice-four* *dice-five* *dice-six*))

;; Menu
;;;;;;;;;

(defparameter *knuppel-title* "    .-. .-')       .-') _               _ (`-.    _ (`-.    ('-.
    \\  ( OO )     ( OO ) )             ( (OO  )  ( (OO  ) _(  OO)
    ,--. ,--. ,--./ ,--,' ,--. ,--.   _.`     \\ _.`     \\(,------.,--.
    |  .'   / |   \\ |  |\\ |  | |  |  (__...--''(__...--'' |  .---'|  |.-')
    |      /, |    \\|  | )|  | | .-') |  /  | | |  /  | | |  |    |  | OO )
    |     ' _)|  .     |/ |  |_|( OO )|  |_.' | |  |_.' |(|  '--. |  |`-' |
    |  .   \\  |  |\\    |  |  | | `-' /|  .___.' |  .___.' |  .--'(|  '---.'
    |  |\\   \\ |  | \\   | ('  '-'(_.-' |  |      |  |      |  `---.|      |
    `--' '--' `--'  `--'   `-----'    `--'      `--'      `------'`------'")

(defparameter *title-line* "───────────────────────────────────────────────────────────────────────")

(defparameter *players-line* "── Spieler ────────────────────────────────────────────────────────────")

(defparameter *title-players* "Angemeldete Spieler:")

(defparameter *title-game-files* "Gespeicherte Spiele:")

(defparameter *title-footer* "von Peter Stoldt")

(defparameter *title-menu-list* '("|+| Spieler hinzufügen"
                                  "|-| Spieler entfernen"
                                  "|l| Spiel laden"
                                  "|s| Spiel speichern"
                                  "|x| Spiel starten"
                                  "|q| Beenden"))

(defparameter *game-line* "─────────────────────────────────────────────────────────────────────────────────────")

;; Score Card
;;;;;;;;;;;;;;;

(defparameter *scard-format*
  "    ┌───────────────────────────────────────────────────────────────────────────────┐
    │ ~77:@<~a~> │
    ├─────────────────┬──────────────╥─────┬╥┬─────────────────┬──────────────╥─────┤
    │ [1]   1 1 1     │ nur Einser   ║ ~3@a │║│ [d] Dreierpasch │ Alle Augen   ║ ~3@a │
    ├─────────────────┼──────────────╫─────┤║├─────────────────┼──────────────╫─────┤
    │ [2]   2 2 2     │ nur Zweier   ║ ~3@a │║│ [v] Viererpasch │ Alle Augen   ║ ~3@a │
    ├─────────────────┼──────────────╫─────┤║├─────────────────┼──────────────╫─────┤
    │ [3]   3 3 3     │ nur Dreier   ║ ~3@a │║│ [f] Full-House  │ 25 Punkte    ║ ~3@a │
    ├─────────────────┼──────────────╫─────┤║├─────────────────┼──────────────╫─────┤
    │ [4]   4 4 4     │ nur Vierer   ║ ~3@a │║│ [s] Kl. Straße  │ 30 Punkte    ║ ~3@a │
    ├─────────────────┼──────────────╫─────┤║├─────────────────┼──────────────╫─────┤
    │ [5]   5 5 5     │ nur Fünfer   ║ ~3@a │║│ [g] Gr. Straße  │ 40 Punkte    ║ ~3@a │
    ├─────────────────┼──────────────╫─────┤║├─────────────────┼──────────────╫─────┤
    │ [6]   6 6 6     │ nur Sechser  ║ ~3@a │║│ [k] Knuppel     │ 50 Punkte    ║ ~3@a │
    ╞═════════════════╪══════════════╬═════╡║├─────────────────┼──────────────╫─────┤
    │     gesamt      │              ║ ~3d │║│ [c] Chance      │ Alle Augen   ║ ~3@a │
    ╞═════════════════╪══════════════╬═════╡║╞═════════════════╪══════════════╬═════╡
    │     Bonus       │ +35 bei >62  ║ ~3d │║│     ges. rechts │              ║ ~3d │
    ╞═════════════════╪══════════════╬═════╡║╞═════════════════╪══════════════╬═════╡
    │     ges. links  │              ║ ~3d │║│     Endsumme    │          --> ║ ~3d │
    ╘═════════════════╧══════════════╩═════╧╩╧═════════════════╧══════════════╩═════╛")

(defparameter *bonus-val* 35)
(defparameter *full-house-val* 25)
(defparameter *small-straight-val* 30)
(defparameter *large-straight-val* 40)
(defparameter *knuppel-val* 50)

(defparameter *sc-fields* (list #\1 #\2 #\3 #\4 #\5 #\6 #\d #\v #\f #\s #\g #\k #\c))

;; Player
;;;;;;;;;;;

(defvar *players* nil)

(defstruct scorecard
  name
  ;; dice columns
  all-one
  all-two
  all-three
  all-four
  all-five
  all-six
  three-of-a-kind
  four-of-a-kind
  full-house
  small-straight
  large-straight
  knuppel
  chance
  ;; summary columns
  bonus
  sum-left
  sum-all-left
  sum-right
  sum-all
  ;; list of game results
  results)

;; vt100 control sequences
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun vt100-home ()
  (format t "~C[H" #\Esc))

(defun vt100-clear-line ()
  (format t "~C[2K" #\Esc))

(defun vt100-clear-screen ()
  "Clear the screen (terminal) and move cursor to the top position."
  (format t "~C[2J" #\Esc)
  (vt100-home))

(defun vt100-cursor-up-and-clear (n)
  "vt100: Move cursor up n times and clear the lines."
  (dotimes (_ n)
    (format t "~C[1A" #\Esc)
    (vt100-clear-line)))

(defun vt100-bold (text)
  "vt100: Return text ready to be printed in bold on the terminal."
  (format nil "~C[1m~a~C[0m" #\Esc text #\Esc))

;; Utils
;;;;;;;;;;

(defun collect-range (max &key (min 0) (step 1))
  (loop for n from min to max by step
        collect n))

;; Load & Save
;;;;;;;;;;;;;;;;

(defun list-game-files (game-files)
  "List files of saved games."
  (let ((seq-num 0))
    (format t "~4@t~a~%~%" *title-game-files*)
    (dolist (file game-files)
      (incf seq-num)
      (format t "~6@t~d. ~a~%" seq-num (file-namestring file)))
    (terpri)))

(defun save-game ()
  "Save a game for later re-use."
  (ensure-directories-exist *files-dir*)
  (let ((game-name (prompt-read-line "Name des Spiels?:")))
    (unless (string= game-name "")
      (with-open-file (out (concatenate 'string *files-dir* game-name)
                           :direction :output
                           :if-exists :supersede)
        (with-standard-io-syntax
          (print *players* out))))))

(defun load-game ()
  "Load a game from file."
  (ensure-directories-exist *files-dir*)
  (let ((game-files (directory (concatenate 'string *files-dir* "*.*"))))
    (when (= 0 (length game-files))
      (prompt-read-line (format nil "Ups...: Keine gespeicherten Spiele gefunden! Drücke [ENTER]:"))
      (return-from load-game))
    (list-game-files game-files)
    (let* ((gnum-str (prompt-read-line "Welches Spiel? (Nummer):"))
           (game-num (parse-integer gnum-str :junk-allowed t)))
      (if (not game-num)
          (prompt-read-line (format nil "Ups...: Bitte eine Nummer eingeben! Drucke [ENTER]:"))
          (if (and (> game-num 0) (<= game-num (length game-files)))
              (with-open-file (in (nth (1- game-num) game-files))
                (with-standard-io-syntax (setf *players* (read in))))
              (prompt-read-line (format nil "Ups...: Spiel ~a gibt es nicht! Drucke [ENTER]:" game-num)))))))

;; Knuppel
;;;;;;;;;;;;

(defun print-game-line ()
  (format t "~2@t~a~%" *game-line*))

(defun prompt-read-line (prompt)
  "Print a prompt and read input from console."
  (format *query-io* "~4@t~a " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-read-char (prompt)
  "Print a prompt and read a character from console."
  (let ((input (prompt-read-line prompt)))
    (if (> (length input) 0)
        (elt input 0)
        #\<)))

(defun list-players ()
  "Print a list of registered players."
  (when *players*
    (format t "~4@t~a~%~%" *title-players*)
    (let ((seq-num 0))
      (dolist (sc *players*)
        (incf seq-num)
        (format t "~6@t~d. ~a~%" seq-num (scorecard-name sc)))
      (terpri))))

(defun list-players-and-scores ()
  "Print a list of registered players and its game results of the last games."
  (when *players*
    (let ((max-plen 0))
      (dolist (sc *players*) ;; Find player with the longest name
        (when (> (length (scorecard-name sc)) max-plen)
          (setf max-plen (length (scorecard-name sc)))))
      (format t "~4@t~a~%~%" *players-line*)
      (let* ((seq-num 0)
             (num-results (length (scorecard-results (first *players*))))
             ;; width is 4 + 71
             (indent (floor (/ (- 75 max-plen 3 (* 4 num-results) 4) 2)))
             ;; players and results -> 4 | max-plen | 3 | res-len * (3+1) | 1 | res-sum
             (name-entry-form (concatenate 'string
                                           "~" (format nil "~d" indent) "@t"    ; indentation
                                           "~" (format nil "~d" max-plen) "a"   ; player name
                                           "   ~{~3d ~}"                        ; sequence of results
                                           " ~4d~%"))                           ; ∑ of results
             (index-form (concatenate 'string
                                      "~" (format nil "~d" (+ indent max-plen 2)) "@t" ; indentation
                                      "~{ ~2d ~}"                                      ; index
                                      "   sum~%")))
        (format t index-form (collect-range (length (scorecard-results (first *players*))) :min 1 :step 1))
        (dolist (sc *players*)
          (incf seq-num)
          (format t name-entry-form (scorecard-name sc)
                  (scorecard-results sc) (reduce #'+ (scorecard-results sc))))
        (terpri)))))

(defun add-player (name)
  "Add a new player (scrorecard) to the list of players."
  (let ((players (reverse *players*)))
    (push (make-scorecard :name name
                          :all-one nil
                          :all-two nil
                          :all-three nil
                          :all-four nil
                          :all-five nil
                          :all-six nil
                          :three-of-a-kind nil
                          :four-of-a-kind nil
                          :full-house nil
                          :small-straight nil
                          :large-straight nil
                          :knuppel nil
                          :chance nil
                          :bonus 0
                          :sum-left 0
                          :sum-all-left 0
                          :sum-right 0
                          :sum-all 0
                          :results nil)
          players)
    (setf *players* (reverse players))))

(defun add-a-player ()
  "Ask for a player name and create the player."
  (let ((player-name (prompt-read-line "Name:")))
    (unless (string= "" player-name)
      (add-player player-name))))

(defun remove-player (name)
  "Remove a player (scorecard) from the list of players."
  (format t "Lösche Spieler ~a...~%" name)
  (setf *players*
        (remove-if #'(lambda (sc) (string= (scorecard-name sc) name)) *players*))
  (list-players))

(defun remove-a-player ()
  "Print a list of players, get a player number and remove the player from the list."
  (let ((players-count (length *players*))
        (player-num))
    (when (<= players-count 0)
      (format t "Ups...: Keine registrierten Spieler vorhanden!")
      (return-from remove-a-player nil))
    (list-players)
    (let ((pnum-str (prompt-read-line "Welcher Spieler? (Nummer):")))
      (unless (string= "" pnum-str)
        (setf player-num (parse-integer pnum-str))
        (if (and (> player-num 0) (<= player-num players-count))
            (remove-player (scorecard-name (nth (1- player-num) *players*)))
            (prompt-read-line (format nil "Ups...: Spieler ~a gibt es nicht! Drücke [ENTER]:" player-num)))))))

(defun roll-n-dice (n)
  "Roll 'n' dice and return a list of sorted values."
  (let ((values nil))
    (dotimes (_ n)
      (push (1+ (random 6)) values))
    (sort values #'<)))

(defun rolled-dice (values)
  "Returns a list of dice string representations according to the values."
  (mapcar (lambda (x) (elt *dice* (1- x))) values))

(defun print-dice (values)
  "Print dice in a line according to the values."
  (let ((dice (rolled-dice values)))
    (dotimes (i (length *dice-one*))
      (format t "~2@t")
      (dotimes (k (length dice))
        (format t "  ~a" (elt (elt dice k) i)))
      (format t "~%"))))

(defun print-menu ()
  "Print the title menu inclusive a list of all players."
  (format t *knuppel-title*)
  (terpri)
  (format t "~4@t~a~%" *title-line*)
  (format t "~4@t~a~66@a~%" *tool-version* *title-footer*)
  (terpri)
  (format t "~{~30@t~a~%~}" *title-menu-list*)
  (terpri)
  (list-players-and-scores)
  (format t "~4@t~a~%" *title-line*))

(defun clear-scorecard (sc)
  "Reset all records in a scorecard."
  (setf (scorecard-all-one sc) nil
        (scorecard-all-two sc) nil
        (scorecard-all-three sc) nil
        (scorecard-all-four sc) nil
        (scorecard-all-five sc) nil
        (scorecard-all-six sc) nil)
  (setf (scorecard-sum-left sc) 0
        (scorecard-bonus sc) 0
        (scorecard-sum-all-left sc) 0)
  (setf (scorecard-three-of-a-kind sc) nil
        (scorecard-four-of-a-kind sc) nil
        (scorecard-full-house sc) nil
        (scorecard-small-straight sc) nil
        (scorecard-large-straight sc) nil
        (scorecard-knuppel sc) nil
        (scorecard-chance sc) nil)
  (setf (scorecard-sum-right sc) 0
        (scorecard-sum-all sc) 0))

(defun init-scorecards ()
  "Reset all scorecards for a new game and make sure that the has a new entry for the current game."
  (dolist (sc *players*)
    (clear-scorecard sc)
    (setf (scorecard-results sc) (append (scorecard-results sc) (list 0)))
    (when (> (length (scorecard-results sc)) 10)
      (setf (scorecard-results sc) (rest (scorecard-results sc))))))

(defun sc-value (v)
  "Return the value of a scorcard entry or '' when nil"
  (if v v ""))

(defun sc-value-or-0 (v)
  "Return the value of a scorcard entry or '' when nil"
  (if v v 0))

(defun update-scorecard (sc)
  "Update the score card of a player by summarizing the points."
  (setf (scorecard-sum-left sc)
        (+ (sc-value-or-0 (scorecard-all-one sc))
           (sc-value-or-0 (scorecard-all-two sc))
           (sc-value-or-0 (scorecard-all-three sc))
           (sc-value-or-0 (scorecard-all-four sc))
           (sc-value-or-0 (scorecard-all-five sc))
           (sc-value-or-0 (scorecard-all-six sc))))
  (setf (scorecard-bonus sc)
        (if (> (scorecard-sum-left sc) 62) *bonus-val* 0))
  (setf (scorecard-sum-all-left sc)
        (+ (scorecard-sum-left sc) (scorecard-bonus sc)))
  (setf (scorecard-sum-right sc)
        (+ (sc-value-or-0 (scorecard-three-of-a-kind sc))
           (sc-value-or-0 (scorecard-four-of-a-kind sc))
           (sc-value-or-0 (scorecard-full-house sc))
           (sc-value-or-0 (scorecard-small-straight sc))
           (sc-value-or-0 (scorecard-large-straight sc))
           (sc-value-or-0 (scorecard-knuppel sc))
           (sc-value-or-0 (scorecard-chance sc))))
  (setf (scorecard-sum-all sc)
        (+ (scorecard-sum-all-left sc) (scorecard-sum-right sc)))
  ;; put result into result list
  (setf (car (last (scorecard-results sc))) (scorecard-sum-all sc)))

(defun sum-of-val (num dice)
  "Retrun the sum of one value of a set of dice."
  (reduce #'+ (map 'list (lambda (x) (if (= x num) num 0)) dice)))

;; https://codereview.stackexchange.com/questions/223123/count-frequencies-of-symbols-in-a-list
(defun occurrences (dice)
  "Return a plist of occurrences of a vlaue in a set of dice like ((val . num) ...)."
  (let ((table (make-hash-table)))
    (loop :for e :in dice
          :do (incf (gethash e table 0)))
    (sort (loop :for k :being :the :hash-key :of table
                  :using (hash-value v)
                :collect (cons k v))
          #'>= :key #'cdr)))

(defun record-one (sc dice)
  "Record all one."
  (setf (scorecard-all-one sc) (sum-of-val 1 dice)))

(defun record-two (sc dice)
  "Record all two."
  (setf (scorecard-all-two sc) (sum-of-val 2 dice)))

(defun record-three (sc dice)
  "Record all three."
  (setf (scorecard-all-three sc) (sum-of-val 3 dice)))

(defun record-four (sc dice)
  "Record all four."
  (setf (scorecard-all-four sc) (sum-of-val 4 dice)))

(defun record-five (sc dice)
  "Record all five."
  (setf (scorecard-all-five sc) (sum-of-val 5 dice)))

(defun record-six (sc dice)
  "Record all six."
  (setf (scorecard-all-six sc) (sum-of-val 6 dice)))

(defun record-three-of-a-kind (sc dice)
  "Record three of a kind."
  (setf (scorecard-three-of-a-kind sc)
        (if (>= (cdr (nth 0 (occurrences dice))) 3)
            (reduce #'+ dice)
            0)))

(defun record-four-of-a-kind (sc dice)
  "Record four of a kind."
  (setf (scorecard-four-of-a-kind sc)
        (if (>= (cdr (nth 0 (occurrences dice))) 4)
            (reduce #'+ dice)
            0)))

(defun record-full-house (sc dice)
  "Record a full house - three of a kind plus two of a kind."
  (setf (scorecard-full-house sc)
        (let ((occur (occurrences dice)))
          (if (and (= (length occur) 2)
                   (= (cdr (nth 0 occur)) 3))
              25
              0))))

(defun record-small-straight (sc dice)
  "Record a small straight - 1-2-3-4 or 2-3-4-5 or 3-4-5-6."
  (let ((sequences '((1 2 3 4) (2 3 4 5) (3 4 5 6))))
    (setf (scorecard-small-straight sc) 0)
    (loop :for sq :in sequences
          :do (when (subsetp sq dice)
                (setf (scorecard-small-straight sc) 30)))))

(defun record-large-straight (sc dice)
  "Record a large straight - 1-2-3-4-5 or 2-3-4-5-6."
  (let ((sequences '((1 2 3 4 5) (2 3 4 5 6))))
    (setf (scorecard-large-straight sc) 0)
    (loop :for sq :in sequences
          :do (when (subsetp sq dice)
                (setf (scorecard-large-straight sc) 40)))))

(defun record-knuppel (sc dice)
  "Record a kuppel - five of a kind."
  (setf (scorecard-knuppel sc)
        (if (= (cdr (nth 0 (occurrences dice))) 5)
            50
            0)))

(defun record-chance (sc dice)
  "Record a chance - count all points."
  (setf (scorecard-chance sc) (reduce #'+ dice)))

(defun print-scorecard (sc)
  "Print the score card of a player."
  (format t *scard-format*
          (scorecard-name sc)
          (sc-value (scorecard-all-one sc)) (sc-value (scorecard-three-of-a-kind sc))
          (sc-value (scorecard-all-two sc)) (sc-value (scorecard-four-of-a-kind sc))
          (sc-value (scorecard-all-three sc)) (sc-value (scorecard-full-house sc))
          (sc-value (scorecard-all-four sc)) (sc-value (scorecard-small-straight sc))
          (sc-value (scorecard-all-five sc)) (sc-value (scorecard-large-straight sc))
          (sc-value (scorecard-all-six sc)) (sc-value (scorecard-knuppel sc))
          (scorecard-sum-left sc) (sc-value (scorecard-chance sc))
          (scorecard-bonus sc) (scorecard-sum-right sc)
          (scorecard-sum-all-left sc) (scorecard-sum-all sc))
  (fresh-line))

(defun print-game-result ()
  "Print the sum of points of the game for all players."
  (print-game-line)
  (format t "~4@tErgebnis:~%")
  (terpri)
  (loop :for sc :in *players*
        :do (format t "~8@t~a: ~3d~%" (scorecard-name sc) (scorecard-sum-all sc)))
  (print-game-line)
  (fresh-line)
  (prompt-read-char "Drücke eine Taste")
  (terpri))

(defun sc-val-of-selection (sc selection)
  "Return scorecard value of selected field."
  (case selection
    ((#\1) (scorecard-all-one sc))
    ((#\2) (scorecard-all-two sc))
    ((#\3) (scorecard-all-three sc))
    ((#\4) (scorecard-all-four sc))
    ((#\5) (scorecard-all-five sc))
    ((#\6) (scorecard-all-six sc))
    ((#\d) (scorecard-three-of-a-kind sc))
    ((#\v) (scorecard-four-of-a-kind sc))
    ((#\f) (scorecard-full-house sc))
    ((#\s) (scorecard-small-straight sc))
    ((#\g) (scorecard-large-straight sc))
    ((#\k) (scorecard-knuppel sc))
    ((#\c) (scorecard-chance sc))))

(defun select-rec-field (sc)
  "Ask the user in which filed the dice should be recorded. Allow only valid fileds."
  (let ((selection nil)
        (retry t))
    (loop :while retry
          :do (setf selection (prompt-read-char "In Feld:"))
              (cond
                ((not (member selection *sc-fields*))
                 (format t "~4@tUps... Feld '~c' gibt es nicht...~%" selection))
                ((sc-val-of-selection sc selection)
                 (format t "~4@tAlso echt jetzt... - Feld '~c' ist doch schon belegt.~%" selection))
                (t (setf retry nil))))
    selection))

(defun enter-points (sc dice)
  "Enter the points into the scorecard of the player."
  (print-game-line)
  (format t "~4@tBitte trage deine Punkte ein.~%")
  (case (select-rec-field sc)
    ((#\1) (record-one sc dice))
    ((#\2) (record-two sc dice))
    ((#\3) (record-three sc dice))
    ((#\4) (record-four sc dice))
    ((#\5) (record-five sc dice))
    ((#\6) (record-six sc dice))
    ((#\d) (record-three-of-a-kind sc dice))
    ((#\v) (record-four-of-a-kind sc dice))
    ((#\f) (record-full-house sc dice))
    ((#\s) (record-small-straight sc dice))
    ((#\g) (record-large-straight sc dice))
    ((#\k) (record-knuppel sc dice))
    ((#\c) (record-chance sc dice))))

(defun selected-dice-ok-p (sel-dice)
  "Check if the list of dice, entered by the user, is OK"
  (let ((len (length sel-dice))) ; Length of nil is 0
    (and (< len 6)
         (zerop (count nil sel-dice))
         (every (lambda (it) (< it 7)) sel-dice)
         (= len (length (remove-duplicates sel-dice))))))

(defun get-selected-dice (pass &optional repeat)
  "Get the list of dice to re-throw from the user and check if the list is OK."
  (let ((sel-dice (loop :for c :across (prompt-read-line (format nil "Welche erneut würfeln [~a/3]:" (+ 2 pass)))
                        :collect (parse-integer (string c) :junk-allowed t))))
    (unless repeat (terpri))
    (if (selected-dice-ok-p sel-dice)
        sel-dice
        (progn
          (vt100-cursor-up-and-clear 2)
          (format t "~4@tUps... das geht so nicht! Wähle Würfel für neuen Wurf, z.B. 135 für den 1., 3. und den 5. Würfel.~%")
          (get-selected-dice pass t)))))

(defun throw-selected-dice (dice pass)
  "Define which dice to throw again and return a new list of dice."
  (let ((sel-dice-raw (get-selected-dice pass)))
    (if sel-dice-raw
        (let* ((selected-dice (remove nil sel-dice-raw))
               (new-dice (roll-n-dice (length selected-dice)))
               (keep-dice (loop :for i :below (length dice)
                                :when (not (member (1+ i) selected-dice))
                                  :collect (nth i dice))))
          (sort (append keep-dice new-dice) #'<))
        dice)))

(defun run-game ()
  (init-scorecards)
  (dotimes (round 13)
    (loop :for sc :in *players* :do
      (vt100-clear-screen)
      (format t "~%~4@tRunde ~d: ~a ist dran:~%" (1+ round) (scorecard-name sc))
      (print-scorecard sc)
      (fresh-line)
      (print-game-line)
      (fresh-line)
      ;; First throw
      (let ((dice (roll-n-dice 5)))
        (print-dice dice)
        (dotimes (pass 2)
          (setf dice (throw-selected-dice dice pass))
          (vt100-cursor-up-and-clear 7)
          (print-dice dice))
        (enter-points sc dice)
        (update-scorecard sc)
        (vt100-clear-screen)
        (terpri) (terpri) ;; Why two of them?
        (print-scorecard sc)
        (print-game-line)
        (prompt-read-char "Weiter mit |ENTER|"))))
  (print-game-result))

;; Main
;;;;;;;;;

(defun main ()
  (sb-ext:disable-debugger)
  (loop
    (vt100-clear-screen)
    (print-menu)
    (let ((selection (prompt-read-char "->")))
      (cond
        ((char= selection #\+) (add-a-player))
        ((char= selection #\-) (remove-a-player))
        ((char= selection #\l) (load-game))
        ((char= selection #\s) (save-game))
        ((char= selection #\x) (run-game))
        ((char= selection #\q) (return-from main))))))

;; Room for tests
;;;;;;;;;;;;;;;;;;;

(defun create-test-players ()
  (setf *players* nil)
  (add-player "Dr. Oetker")
  (add-player "Wusel")
  (add-player "Hans Otto Lilienthal"))

(defun clear-test-players ()
  (setf *players* nil))

(defun test-run ()
  (create-test-players)
  (run-game))

(defun test-sc ()
  ;;(add-player "Hans Otto Lilienthal")
  (dotimes (i 3)
    (init-scorecards)
    (let ((sc (nth 0 *players*)))
      (setf (scorecard-all-one sc) 4)
      (setf (scorecard-all-two sc) 8)
      (setf (scorecard-all-three sc) 12)
      (setf (scorecard-all-four sc) 16)
      (setf (scorecard-all-five sc) 20)
      (setf (scorecard-all-six sc) 6)
      (setf (scorecard-three-of-a-kind sc) 15)
      (setf (scorecard-four-of-a-kind sc) 16)
      (setf (scorecard-full-house sc) 25)
      (setf (scorecard-small-straight sc) 30)
      (setf (scorecard-large-straight sc) 0)
      (setf (scorecard-knuppel sc) 0)
      (setf (scorecard-chance sc) 12)
      (update-scorecard sc)
      (print-scorecard sc)
      (format t "Results: ~a~%" (scorecard-results sc)))))

(defun test-menu ()
  (create-test-players)
  (print-menu)
  (setf *players* nil))

(defun test-dice ()
  (dotimes (n 14)
    (print-dice (roll-n-dice n))))

;; (create-test-players)
;; (clear-test-players)
;; (test-run)
;; (test-sc)
;; (test-menu)
;; (test-dice)
